# BeyondCell has moved.

## You can now find the active repo at https://github.com/cnio-bu/beyondcell

For details on why we moved please see [here](https://forum.gitlab.com/t/gitlab-introduces-user-limits-for-free-users-on-saas/64288/27?u=tdido)
